<?php

/**
 * Forms for ABCRC Plugin
**/


class Abcrc_Forms {
	
	function Abcrc_Forms()
	{
		echo "
		<!-- Styles -->
		<style type=\"text/css\">
		/*	div.abcrc_wrap { padding: 15px; }
			p.instructions { font-style: italic; padding: 10px 0;}
			table.list { width: 100%; border-collapse: collapse; }
			table.list td, table.list th {  font-size: 11px; text-align: left; padding: 2px 5px 0 0;}
			table.list th { font-weight: bold; font-size: 12px; border-bottom: 1px solid #000; }*/
			td { empty-cells: show; }
			#wpbody-content fieldset { border: 1px solid #CCC; padding: 10px; margin: 0 0 10px 0; }
			#wpbody-content label { display: block; padding-top: 5px; }
			.search_form { float: left; width: 300px }
			.total_results { float: right; padding-top: 30px;  }
		</style>";
	}
	
	
	 
	
	function login()
	{
		$fieldsets = array(
			array(
				'type' 		=> 'fieldset',
				'legend' 	=> 'Please Enter Your Access Code',
				'id'		=> 'login',
				'class'		=> 'right_labels',
				'items'		=> array(
				
					array(
						'name'	=> 'Password',
						'type'	=> 'password'
					)
				)
			)
		);
		
		return $this->render($fieldsets);
	}
	
	
	
	function user($defaults = array())
	{
		$user = new Abcrc_User();
		$profile = new Abcrc_Profile();
		
		$fieldsets = array(
			array(
				'type' 		=> 'fieldset',
				'legend' 	=> 'Account Information',
				'id'		=> 'account_info',
				'items'		=> array(
					array(
						'type' 	=> 'hidden',
						'name'	=> 'UserID'
					),
					 
					array(
						'type' 	=> 'text',
						'name' 	=> 'Password'
					),
					array(
						'name'	=> 'Active',
						'label' => 'Active',
						'type'  => 'select',
						'options' => array(0=>'No', 1=>'Yes')
					)
				)
			),
			array(
				'type' => 'fieldset',
				'legend' => 'Contact Details',
				'id'	=> 'contact',
				'items' => array(
					array(
						'name' 	=> 'Name',
						'label'	=> 'Name'
					),
					 
					array(
						'name' 	=> 'Email',
						'label' => 'Email Address'
					),
				)
			)
			 

		);
		
    return $this->render($fieldsets, $defaults);
	}
	
	function render ($fieldsets, $defaults = array())
	{
		$html = '';
		
		foreach ($fieldsets as $f)
		{
			if (isset($f['class']))
				$class = $f['class'];
			else
				$class = '';
			
			if (isset($f['id']))
				$html .= "<fieldset id=\"{$f['id']}\" class=\"{$class}\">";
			else
				$html .= "<fieldset class=\"{$class}\">";
				
			if (isset($f['legend']))
				$html .= "<legend>{$f['legend']}</legend>";
				
			foreach ($f['items'] as $i)
			{
				
				$default = $this->get_default($i['name'], $defaults);
				
				if (isset($i['label']))
					$label = $i['label'];
				else
					$label = ucwords($i['name']);
				
				$html .= '';
				
				if (isset($i['type']))
					$type = $i['type'];
				else
					$type = 'text';
				
				switch ($type)
				{
					default:
					case 'text':
						$html .= "<div class=\"form-field\"><label for=\"{$f['id']}_{$i['name']}\">{$label}:</label> <input type=\"text\" name=\"{$i['name']}\" value=\"$default\" id=\"{$f['id']}_{$i['name']}\"></div>";
						break;
						
					case 'password':
						$html .= "<label for=\"{$f['id']}_{$i['name']}\" class='sr-only'>{$label}:</label> <input class='form-control' placeholder='Access Code' type=\"password\" name=\"{$i['name']}\" id=\"{$f['id']}_{$i['name']}\">";
						break;
				
					case 'textarea':
						$html .= "<div class=\"form-field\"><label for=\"{$f['id']}_{$i['name']}\">{$label}:</label><textarea  name=\"{$i['name']}\" id=\"{$f['id']}_{$i['name']}\">{$default}</textarea></div>";
						break;
				
					case 'checkbox':
					
						break;
						
					case 'clear':
						$html .= "<div class=\"clear\"><span></span></div>";
						break;
						
						
					case 'hidden':
						$html .= "<input type=\"hidden\" name=\"{$i['name']}\" value=\"{$default}\">";
						break;
				
					case 'select':
						$html .= "<div class=\"form-field\"><label for=\"{$f['id']}_{$i['name']}\">{$label}:</label><select id=\"{$f['id']}_{$i['name']}\" name=\"{$i['name']}\">";
						$html .= $this->render_options($i['options'], $default);
						$html .= "</select></div>";
						break;
						
					case 'note':
						$html .= "<p class=\"note\">{$i['text']}</p>";
						break;
					
				}
			} 
			
			$html .= '</fieldset>';
			
		}
		
		
		
		return $html;
	}
	
	function render_options ($list, $selected)
	{
		$options = '';
		foreach ($list as $value=>$label)
		{
			if ($value == $selected)
				$options .= "<option value=\"$value\" selected>$label</option>";
			else
				$options .= "<option value=\"$value\">$label</option>";
		}
		
		return $options;
	}
	
	function get_default($field, $defaults)
	{
		
		if (isset($defaults[$field]))
			return $defaults[$field];
		else
			return '';
	}
}
