<?php
/**
 * @package autocan_partners
 * @author JMC
 * @version 1.0
 */
/*
Plugin Name: AutoCanada
Plugin URI: http://www.ideasthattrigger.com/#
Description: AutoCanada Custom Plugins
Author: JMC
Version: 1.0
Author URI: http://www.ideasthattrigger.com
*/

error_reporting(E_ALL);
ini_set('display_errors', '1');

require (realpath(dirname(__FILE__)).'/inc/abcrc.class.php');
require (dirname(__FILE__).'/abcrc_forms.class.php');

add_action('admin_menu', 'abcrc_create_menu');

function abcrc_create_menu() 
{
	add_menu_page('ABCRC Settings', 'Partners', 'administrator', 'abcrc_home', 'abcrc_user_editor');
	add_submenu_page('abcrc_home', 'User Manager', 'User Manager', 'administrator', 'abcrc_home', 'abcrc_user_editor');
}

function abcrc_home() 
{
	echo '<div class="wrap">';
	echo '<h2>ABCRC Settings</h2>';
	echo '<p>Select a manager:</p>';
	echo '<p><a href="/wp-admin/admin.php?page=abcrc_user_editor_handle">User Manager</a><br><a href="/wp-admin/admin.php?page=abcrc_profile_editor_handle">Company Manager</a></p>';
	
	
	echo '</div>';
}

function abcrc_user_editor()
{
	echo '<div class="wrap">';
	
	
	$user = new Abcrc_User();
	$forms = new Abcrc_Forms();
	
	if (isset($_GET['action'])) 
		$action = $_GET['action'];
	else
		$action = 'list';
	
	switch ($action)
	{
		default:
	/*	case 'repair':
			$users = $user->find_all();
			echo '<pre>';
			global $wpdb;
			foreach ($users as $u)
			{
				$query = "UPDATE User_List SET Company='{$u['Company']}' WHERE UserID={$u['UserID']}";
				$wpdb->query($query);
			}
			
			//var_dump($users);
			echo 'done.';
			break;*/
		case 'search':
		case 'list':
		
			echo '<h2>AutoCanada Partners</h2>';
		
			echo '<p class="instructions">Click the edit link beside a partner to edit that partner.</p>';
		
			echo '<p><a href="/wp-admin/admin.php?page=abcrc_home&action=add">Add a new Partners</a></p>';
			
			echo $user->search('abcrc_home');
			
			if (($action == 'search') && !isset($_POST['clear']))
				$keywords = $_POST['q'];
			else
				$keywords = false;
			
			echo $user->create_list(array(
				'UserID' => 'User Id',
				'Active'  => 'Active',
				'Name' => 'Name',
				'Email' => 'Email'
				), 
				$user->find_all($keywords),
				array(
					'Active'	=> 'y_n'
				), 
				$keywords);
		
			break;
			
		case 'add':
				echo '<h2>Add New Partner</h2>';

				echo '<form method="post" action="/wp-admin/admin.php?page=abcrc_home&action=save">';
				$form = $forms->user();
				echo $form;
				
				echo '<p class="submit"><input class="button-primary" type="submit" value="Submit" name="Submit"> <a href="/wp-admin/admin.php?page=abcrc_home">Cancel</a></p>';
				
				echo '</form>';

				break;
			
		case 'edit':
		
			echo '<h2>Edit ABCRC User</h2>';
		
			$id = (int)$_GET['id'];
			$user_info = $user->find_by_id($id);
      echo '<form method="post" action="/wp-admin/admin.php?page=abcrc_home&action=save">';
			$form = $forms->user($user_info);
			echo $form;
			
			echo '<p class="submit"><input class="button-primary" type="submit" value="Submit" name="Submit"> <a href="/wp-admin/admin.php?page=abcrc_home">Cancel</a></p>';
			echo '</form>';
		
			break;
		
		case 'save':
			echo '<h2>Partner Saved</h2>';
			if (empty($_POST['UserID'])){
				$results = $user->create($_POST);
			}else{
				$results = $user->update($_POST['UserID'], $_POST);
		  }
			if ($results !== false)
				echo '<p>User saved successfully. <a href="/wp-admin/admin.php?page=abcrc_home">Click here</a> to return to user list.</p>';
			else
				echo '<p class="error">There was a problem saving the user. Please contact the system administrator.</p>';
			
			
			break;
			
		case 'delete':
			
			$results = $user->delete($_GET['id']);
			
			if ($results !== false)
				echo '<p>User deleted successfully. <a href="/wp-admin/admin.php?page=abcrc_home">Click here</a> to return to user list.</p>';
			else
				echo '<p class="error">There was a problem deleting the user. Please contact the system administrator.</p>';
		
			break;
	}
	
	echo '</div>';
	
}
?>