<?php

/**
 * Abcrc_User
 *
 * This class provides database helper methods for the WP plugin.
 *
 */

class Abcrc_User extends Abcrc {
    
  function find_all($keywords = false) {
    global $wpdb;
    
    if ($keywords)
      $where = "WHERE wp_partners.Name LIKE '%$keywords%'";
    else
      $where = '';
    
    $sql = "SELECT wp_partners.UserID, 
                   wp_partners.Active,
                   wp_partners.Name
                  
              FROM wp_partners 
         
                   $where
          ORDER BY wp_partners.Name ASC";
    
    return $wpdb->get_results($sql, ARRAY_A);
  }
  
  function find_by_id($id) {
    global $wpdb;
    
    $result = $wpdb->get_row($wpdb->prepare("SELECT  UserID, Name, Password, Email, Active FROM wp_partners WHERE UserID = %d", $id),ARRAY_A );
    
   
    
    return $result ;
  }
  
  function create($fields) {
  
   print_r($fields);
    
    global $wpdb;
    
    unset($fields["UserID"]);
    unset($fields["Submit"]);
    
    foreach($fields as $k=>&$v) {
     $v =   $v;
    }
    
    echo $query = "INSERT INTO wp_partners (".implode(", ", array_keys($fields)).") VALUES ('".implode("', '", array_values($fields))."')";
    $results = $wpdb->query($query);
    
    return $results;
  }
  
  function update ($id, $fields) {
    global $wpdb;
    
    $id = (int)$id;
    unset($fields["UserID"]);
    unset($fields["Submit"]);

    $set = array();
    foreach ($fields as $k=>$v) {
      $set[] = "$k = '".mysql_real_escape_string($v)."'";
    }
    
    $query = 'UPDATE wp_partners SET '.implode(', ', $set).' WHERE UserID='. $id;
    $results = $wpdb->query($query);
    
    return $results;
  }
  
  function delete ($id) {
    global $wpdb;
    return $wpdb->query("DELETE FROM wp_partners WHERE UserID=".(int)$id);
  }
  
}