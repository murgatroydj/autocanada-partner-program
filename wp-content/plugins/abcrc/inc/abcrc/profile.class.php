<?php

/**
 * Abcrc_Profile
 *
 * This class contains helper methods for working with company and user info for the WP backend
 * tools.
 *
 */


class Abcrc_Profile extends Abcrc  {
  
  // generates options for a select list
  function select_list() {
    global $wpdb;
    
    $results = $wpdb->get_results("SELECT ItemID, Company FROM Profile_List ORDER BY Company");
    
    $list = array();
    
    foreach ($results as $r) {
      $list[$r->ItemID] = $r->Company;
    }
    
    return $list; 
  }
  
  function find_all($keywords = false) {
    global $wpdb;
    
    if ($keywords)
      $where = "WHERE Profile_List.Company LIKE '%$keywords%' OR Profile_List.Code LIKE '%$keywords%' OR Profile_List.City LIKE '%$keywords%'";
    else
      $where = '';
    
    $sql = "SELECT  Profile_List.ItemID, 
            Profile_List.Active, 
            Profile_List.Code, 
            Profile_List.Company, 
            Profile_List.City
           FROM Profile_List 
            $where
        ORDER BY Profile_List.Company";

    return $wpdb->get_results($sql, ARRAY_A);
  }
  
  function find_by_id($id) {
    global $wpdb;
    
    return $wpdb->get_row(
      "SELECT Profile_List.*
         FROM Profile_List 
          WHERE Profile_List.ItemID = $id", 
    ARRAY_A);
  }
  
  function find_type($code) {
    global $wpdb;
    
    $results = $wpdb->get_row("SELECT depotType FROM rip_depot_meta  WHERE code = '$code'");    
    return $results;
  }
  
  function create($fields) {
    global $wpdb;
    
    unset($fields["ItemID"]);
    unset($fields["Submit"]);
    
    $results = $wpdb->query("INSERT INTO Profile_List (".implode(", ", array_keys($fields)).") VALUES ('".implode("', '", array_values($fields))."')");
      
    return $results;
  }
  
  function update ($id, $fields) {
    global $wpdb;
    
    $id = (int)$id;
    
    unset($fields["ItemID"]);
    unset($fields["Submit"]);
    
    $set = array();
    foreach ($fields as $k=>$v) {
      $set[] = "$k = '".$v."'";
    }
    
    $results = $wpdb->query('UPDATE Profile_List SET '.implode(', ', $set).' WHERE ItemID='.$id);
    return $results;
  }
  
  function delete ($id) {
    global $wpdb;
    
    $results = $wpdb->query("DELETE FROM Profile_List WHERE ItemID=".(int)$id);
    
    if ($results) {
      $results = $wpdb->query("DELETE FROM User_List WHERE ParentID=".(int)$id);
    } else {
      return false;
    }
  }
  
  // returns options for the user type list
  function ml_select_list () {
    global $wpdb;
    
    $results = $wpdb->get_results("SELECT MLID, Name FROM ML_List ORDER BY Name");
    
    $list = array();
    foreach ($results as $r) {
      $list[$r->MLID] = $r->Name;
    }
    
    return $list;
  }
  
  function get_profile_codes($specific_code = false) {
    global $wpdb;
    
    if ($specific_code)
      $results = $wpdb->get_results("SELECT ItemID, Code, Company FROM Profile_List WHERE Active=1 AND Code='$specific_code' ORDER BY Company", ARRAY_A);
    else
      $results = $wpdb->get_results("SELECT ItemID, Code, Company FROM Profile_List WHERE Active=1 ORDER BY Company", ARRAY_A);
    
    $codes = array();
    
    if ($results) {
      foreach ($results as $r) {
        if (!empty($r['Code']))
          $codes[$r['Code']] = $r;
      }
    }
      
    return $codes;
  }
}