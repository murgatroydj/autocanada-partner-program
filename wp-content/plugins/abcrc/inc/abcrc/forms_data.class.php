<?php
/**
 * Abcrc_Forms_Data
 *
 * This class handles saving the form data to the database as well encrypting and emailing to 
 * the correct person at ABCRC
 *
 */
class Abcrc_Forms_Data extends Abcrc {
  
 function save($form_id, $data, $profile_id = false, $report_id ="", $customer_id="") {
    global $wpdb;
    
    $time = date("Y-m-d H:i:s");
    $data = mysql_escape_string($data);
    if ($profile_id) {
      $wpdb->query("INSERT INTO form_data (form_id, created_on, data, profile_id, report_id, customer_id) VALUES ('$form_id', '$time', '$data', '$profile_id', ' $report_id', '$customer_id')");
    } else {
      $wpdb->query("INSERT INTO form_data (form_id, created_on, data, eport_id, customer_id) VALUES ('$form_id', '$time', '$data', ' $report_id', '$customer_id')");
    }
    
  }
  
  function get_all($form_id) {
    global $wpdb; 
    return $wpdb->get_results("SELECT d.id, d.report_id,d.created_on, d.customer_id, c.Company as company_name
                                 FROM form_data d
                            LEFT JOIN Profile_List c ON d.profile_id = c.ItemID
                                WHERE form_id=$form_id 
                             ORDER BY created_on ASC");
  }
  
  public static function checkPrior($p_companyID, $p_containerID){
    global $wpdb; 
	$results = $wpdb->get_results("SELECT ReportID FROM ExportDetails  WHERE CustomerNumber = '$p_companyID' AND ItemID = '$p_containerID' LIMIT 1");
	return count($results);
  }
  
  function load($form_id) {
    global $wpdb;
    
    return $wpdb->get_row("SELECT * FROM form_data WHERE id=$form_id");
  }
  
  function process_brand_order($vars, $profile_id) {
    global $wpdb;
 
    $testemail = 'arstaff@abcrc.com'; //, james@jamesmurgatroyd.com
  
    $emailsubject = "Encrypted Brand Report Form";
    $emailfrom = "From: ".FORM_EMAIL_FROM;
    $encrypt_id = 'ARStaff <ARStaff@abcrc.com>';
    $start_date = date("Y-m-d", strtotime($vars['reporting_period_to']));

    $sql = "SELECT * FROM container_fee_schedule WHERE start_date < '$start_date' ORDER BY start_date DESC LIMIT 1";
    
	$result = $wpdb->get_row($sql);

	
	
    if ($result == null){
		
		  $schedule_id = 1;	
	}
    
    else{
		  $schedule_id = (int)$result->id;
		
	}
    
    $sql = "SELECT  container_fee.container_id, 
            container.field_name, 
            container.name, 
			container.gpID, 
            container.size, 
            container.icon,
            container_fee.deposit, 
            container_fee.crf, 
            container_group.name AS group_name
           FROM container_fee 
       INNER JOIN container ON container_fee.container_id = container.id
       INNER JOIN container_group ON container.container_group_id = container_group.id
            WHERE container_fee.container_fee_schedule_id=$schedule_id  
            ORDER BY container.container_order ASC";

  $containers = $wpdb->get_results($sql);
  $first_cat = true;
  $deposit_total = 0;
  $crf_total = 0;
  $total_sold = 0;
  $total = 0;
  
  $export_report = new ExporterReport(array(
    'participant' => $vars['brand_owner'],
	'CustomerNumber' => $vars['custome_number'],
    'sales_period_start' => date("Y-m-d H:i:s", strtotime($vars['reporting_period_from'])),
    'sales_period_end' => date("Y-m-d H:i:s", strtotime($vars['reporting_period_to'])),
    'report_date' => date("Y-m-d H:i:s"),
    'completed_by' => $vars['completed_by'],
    'authorized_by' => $vars['authorized_by']
  ));
 
 
 



  $export_items = new ExporterItems();

  $last_group = '';
  foreach ($containers as $c) {
    if ($c->group_name != $last_group) {
      if ($first_cat) {
        $body .= "\r\n".strtoupper($c->group_name);
        $first_cat = false;
      } else {
        $body .= "\r\n\r\n".strtoupper($c->group_name);
      }
    }
    
    $deposit = $c->deposit * (int)$vars[$c->field_name];
    $crf = $c->crf * (int)$vars[$c->field_name];
    $line_total = $deposit + $crf; 
    $sold = (int)$vars[$c->field_name];
    $deposit_total += $deposit;
    $crf_total += $crf;
    $total += $line_total;
    $total_sold += $sold;
  
    $body .= "
{$c->name}\t{$c->size}\t{$sold}\t$".sprintf("%.2f", $c->deposit)."\t$".sprintf("%.2f", $c->crf)."\t$".sprintf("%.2f", $deposit)."\t$".sprintf("%.2f", $crf)."\t$".sprintf("%.2f", $line_total);
  
    $last_group = $c->group_name;
	
	
	
	    $item = new ExporterItem(array(
	      'material' => $c->name,
		  'ItemID' => $c->gpID,
		  'CustomerNumber' => $vars['custome_number'],
	      'size' => $c->size,
	      'containers_sold' => $sold,
	      'deposit_refund' => $c->deposit,
	      'container_recovery_fee' => $c->crf,
	      'deposit_refunds_payable' => $deposit,
	      'crf_payable' => $crf,
	      'total_payable' => $line_total
	    ));
   
    $export_items->add($item);
  }

  $body .= "

TOTALS\t\t{$total_sold}\t\t\t$".sprintf("%.2f", $deposit_total)."\t$".sprintf("%.2f", $crf_total)."\t$".sprintf("%.2f", $total);

  $body .= "
Report date:\t".date("F j, Y \a\\t h:i A")."
Completed by:\t{$vars['completed_by']}
Authorized by:\t{$vars['authorized_by']}
Contact phone:\t{$vars['contact_telephone']}
";
  	
	
	

$report_id = ABCRC_Exporter::export($export_report, $export_items);
$customer_id = $vars['custome_number'];
$head = "ALBERTA BEVERAGE CONTAINER RECYCLING CORPORATION
PARTICIPANT'S SALES REPORT

PARTICIPANT:\t{$vars['brand_owner']}
CUSTOMER ID:\t{$vars['custome_number']}
REPORT ID:\t{$report_id}
SALES PERIOD:\t".date("M j, Y", strtotime($vars['reporting_period_from']))." to ".date("M j, Y", strtotime($vars['reporting_period_to']))."

Material\tSize\tContainers Sold\tDeposit Refund\tContainer Recovery Fee\tDeposit Refunds Payable\tCRF Payable\tTotal Payable";
	
 $msg = $head . $body;
	
  $this->save(2, $msg, $profile_id, $report_id, $customer_id);
    $key_path = GPG_KEYS.'/brand';
    putenv('GNUPGHOME='.$key_path);
    $infile = tempnam("/tmp", "PGP.asc");
    $outfile = $infile.".asc";


	//echo $infile;
    //write form variables to email
    $fp = fopen($infile, "w");
    fwrite($fp, $msg);
    fclose($fp);
	chmod($infile, 0777);

    //set up the gnupg command. Note: Remember to put E-mail address on the gpg keyring.
    $command = "/usr/bin/gpg -a --always-trust --allow-secret-key-import  --keyring '$key_path/pubring.skr' --secret-keyring '$key_path/secring.skr' --recipient '$encrypt_id' --encrypt -o $outfile $infile";

    //execute the gnupg command
    system($command, $result);
	
    //delete the unencrypted temp file
    unlink($infile);
    
  
	
    if ($result==0) {
      $fp = fopen($outfile, "r");

      if(!$fp||filesize ($outfile)==0)  {
       
	 
	    $result = -1;
      } else  {
       
		//read the encrypted file
        $contents = fread ($fp, filesize ($outfile));

        //delete the encrypted file
        unlink($outfile);

        //send the email
        mail ($testemail, $emailsubject, $contents, $emailfrom);

        return true;
      } 
    }

    if($result!=0)  {

       return false;
    }
  }
}