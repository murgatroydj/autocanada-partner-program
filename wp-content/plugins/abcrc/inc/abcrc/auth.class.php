<?php

/**
 * Abcrc_Auth
 *
 * This class provides authentication services for the ABCRC frontend components.
 *
 */

class Abcrc_Auth extends Abcrc {
  
  function Abcrc () {
    $this->__construct();
  } 
  
  function __construct() {
    if (!(session_id()))
      session_start();
  }
  
  function is_authenticated() {
    if (!isset($_SESSION['user_id'])){
       return false;
    } else {
       return true;
    }
      
    return false;
  }
  
  // Login user and save session cookie
  function login($password) {
   
    global $wpdb;
    
    $query = "SELECT UserID, Name
      
    FROM wp_partners
   
    WHERE  Password = '{$password}' AND Active = 1 ";

    $results = $wpdb->get_row($query);
    
     
    
    if ($results) {
     
      $_SESSION['user_id'] = $results->UserID;
      $_SESSION['partner_name'] = $results->Name;
   
      $now = date("Y-m-d H:i:s");
     // $query = "INSERT INTO login_log (created, user_id, company_id) VALUES ('{$now}', '{$results->UserID}', '{$results->ParentID}')";
     // $wpdb->query($query);
      
      return true;
    } else {
      return false;
    }
  }
  
  function get_ids() {
    return array(
      'user_id' => $_SESSION['user_id']
    );
  }
  
    
  function logout() {
    session_unset();
    session_destroy();
  }
  
  
}