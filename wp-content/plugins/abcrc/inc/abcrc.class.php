<?php

/**
 * ABCRC
 * This class contains the logic needed for the ABCRC plugins, login and documents
 *
**/

date_default_timezone_set("America/Edmonton");

$class_root = realpath(dirname(__FILE__)) .'/abcrc/';

require_once($class_root . 'auth.class.php');
require_once($class_root . 'forms_data.class.php');
require_once($class_root . 'profile.class.php');
require_once($class_root . 'user.class.php');

// constants
define ("ABCRC_AUTH_BRAND_OWNER", 6);
define ("ABCRC_AUTH_DEPOT_OPERATOR", 5);
define ("ABCRC_AUTH_CARRIER", 9);

 
 

class Abcrc {

  function create_list($columns, $results, $filter = array(), $keywords = false, $row_actions = false) {
    if ($row_actions === false)
      $row_actions = array('edit' => 'edit', 'delete' => 'delete');
    
    if ($keywords)
      echo '<p class="total_results"><strong>'.count($results).'</strong> results found for <strong>"'.$keywords.'"</strong>.</p>';
    else
      echo '<p class="total_results"><strong>'.count($results).'</strong> results found.</p>';
    
    echo '<table class="widefat page fixed" cellspacing="0">';
    
    // write out column names
    echo '<thead><tr>';    
    if (count($row_actions) > 0) {
      echo '<th width="75">&nbsp;</th>';
    }
    
    foreach ($columns as $c)
      echo "<th>{$c}</th>";
    
    echo '</tr></thead>';
    
    // write out results
    echo '<tbody>';
    
    $alt = 0;
    if (count($results) > 0) {
      foreach ($results as $r) {
        $id = current($r);

        if ($alt%2 == 0)
          echo '<tr>';
        else
          echo '<tr class="alternate">';

        if (count($row_actions) > 0) {
          echo '<td>';
        
          foreach ($row_actions as $action=>$label) {
            if ($action == 'delete')
              $confirm = ' onclick="return confirm(\'Do you really want to delete this item?\')"';
            else
              $confirm = '';
            
            echo'<a href="?page='.$_GET['page'].'&action='.$action.'&id='.$id.'" '.$confirm.'>'.$label.'</a> &nbsp; ';  
          }
          
          echo '</td>';
        }
        
        foreach ($r as $k=>$v) {
          if (array_key_exists($k, $columns)) {
            if (isset($filter[$k])) {
              echo '<td>'.$this->filter($filter[$k], $v).'</td>';
            } else {
              echo "<td>$v</td>";
            }
          }
        }
        
        echo '</tr>';
        $alt ++;
      }
    } else {
      if (count($row_actions) > 0) {
        echo '<tr class="no-results"><td colspan="'.(count($columns) + 1).'">No results found.</td></tr>';
      } else {
        echo '<tr class="no-results"><td colspan="'.(count($columns)).'">No results found.</td></tr>';
      }
    }
        
    echo '</tbody>';
    echo '</table>';
  }
  
  function filter ($type, $string) {
    switch ($type) {
      case 'y_n':
        if ((int)$string == 1)
          return 'Yes';
        else
          return 'No';
      
        break;
    }
  }
  
  function search($page) {
    echo '<form action="/wp-admin/admin.php?page='.$page.'&action=search" method="post" class="search_form">';
    echo '<p><label for="q">Search: </label><input type="text" name="q"> <input type="submit" value="Go"> <input type="submit" name="clear" value="Clear"></p>';
    echo '</form>';
  }  
}