<?php
/**
 * autocan-partners functions and definitions
 *
 * @package autocan-partners
 */


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
 
date_default_timezone_set('MST');


function autocan_partners_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'autocan_partners_content_width', 640 );
}
add_action( 'after_setup_theme', 'autocan_partners_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function autocan_partners_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'autocan-partners' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'autocan_partners_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function autocan_partners_scripts() {
	wp_enqueue_style( 'autocan-partners-style', get_stylesheet_uri() );

	wp_enqueue_script( 'autocan-partners-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'autocan-partners-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'autocan_partners_scripts' );


/**
 * THEME FUNCTIONS
 */


/**
 * Basic theme setup
 */
require_once( get_template_directory() . '/inc/init.php' );
require_once( get_template_directory() . '/inc/base-functions.php' );
require_once( get_template_directory() . '/inc/jmc-dashboard.php' );
require_once( get_template_directory() . '/inc/theme-helpers.php' );

/**
* Define custom post types
*/
require_once( get_template_directory() . '/inc/theme-taxonomies.php' );
require_once( get_template_directory() . '/inc/theme-post-types.php' );

/**
 * Custom functions
 */
require_once( get_template_directory() . '/inc/theme-options.php' );
require_once( get_template_directory() . '/inc/theme-functions.php' );

/**
 * Require Plugins
 */
/*require_once( get_template_directory() . '/inc/class-tgm-plugin-activation.php' );
require_once( get_template_directory() . '/inc/theme-require-plugins.php' );
add_action( 'tgmpa_register', 'om_register_required_plugins' );*/

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Shortcodes
 */
require_once( get_template_directory() . '/inc/theme_shortcodes.php' );


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Customizer additions.
 */
//require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/inc/jetpack.php';


/**
 * disable comments.
 */
require get_template_directory() . '/inc/comments.php';



/**
 * Filter Yoast SEO Metabox Priority
 */
add_filter( 'wpseo_metabox_prio', 'om_filter_yoast_seo_metabox' );
function om_filter_yoast_seo_metabox() {
	return 'low';
}

// FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 95, true);  
//add_filter('frm_get_default_value', 'my_custom_default_value', 10, 2);
function my_custom_default_value($new_value, $field){
  if(isset($_SESSION['entry_id'])){
  if($field->id == 95){ //change 25 to the ID of the field
    $new_value = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 95, true); //change 'default' to your default value
  }
  if($field->id == 104){ //change 25 to the ID of the field
    $new_value = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 104, true); //change 'default' to your default value
  }
  if($field->id == 11){ //change 25 to the ID of the field
    $new_value = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 11, true); //change 'default' to your default value
  }
  if($field->id == 105){ //change 25 to the ID of the field
    $new_value = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 105, true); //change 'default' to your default value
  }
  if($field->id == 106){ //change 25 to the ID of the field
    $new_value = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 106, true); //change 'default' to your default value
  }
  if($field->id == 107){ //change 25 to the ID of the field
    $new_value = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 107, true); //change 'default' to your default value
  }
  if($field->id == 108){ //change 25 to the ID of the field
    $new_value = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 108, true); //change 'default' to your default value
  }
  if($field->id == 109){ //change 25 to the ID of the field
    
      $new_value = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 109, true); //change 'default' to your default value
  }
  }
  return $new_value;
}