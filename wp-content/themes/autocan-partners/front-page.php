<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package autocan-partners
 */
 
$auth = new Abcrc_Auth();

if (isset($_GET['login'])){
  $login_ok = $auth->login($_POST['Password']);
}


$logedin = $auth->is_authenticated();

if (isset($_GET['logout'])){
	$login_ok = $auth->logout();
}
	
//print_r($_SESSION);


get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
      <div class="container">
        <div class="panel">
          
				<?php
          
        // If login error show error
				if (isset($login_ok) && !$login_ok)
				echo '<p class="errors" style="display: block">Username or password incorrect.</p>';
          
        // if not logged in show login form
        if(!$logedin): ?>
        
        <form class="form-signin" method="post" action="?login">
          <?php
    				$forms = new Abcrc_Forms();
            echo $forms->login();
    		  ?>
    			
    			 <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="Submit">Submit</button>
    			 
    		</form>
        
      <?php endif;?>
          
           
          <!-- post loop -->
          <?php if($logedin): ?>
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="row">
              <div class="col-xs-12">
                
                <h3><?php the_title(); ?></h3>
            
                <?php the_content(); ?>
                
                <a href="/index.php/inside-page/" class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="Submit">Start</a>
              </div>
            </div>
            
          <?php endwhile; endif; ?>
          <?php endif;?>
          <!-- End post loop -->

		  </div>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
	
  
<?php get_footer( 'home' ); ?>
