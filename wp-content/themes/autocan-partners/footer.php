<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package autocan-partners
 */

?>

	</div><!-- #content -->

<footer class="footer-wrapper" role="contentinfo">
	<div class="footer-container container">
    <div class="row">
           <div class="col-xs-12">
             <p class="legal">Offer is subject to number of remaining models in stock and dealer discretion. Offer does not include any specialty vehicles including but not limited to Hellcat models, Corvette models, and custom model packages. See dealer for details.</p>
           </div>
		</div>
  </div>
</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

<script src="https://maps.googleapis.com/maps/api/js"></script>
 
<script>
 function initialize() {
    //var myLatlng = new google.maps.LatLng(53.608598,-113.5477786);
    
    var mapCanvas = document.getElementById('map-canvas');
    
    var mapOptions = {
      //center: new google.maps.LatLng(53.608598,-113.5477786),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    var bounds = new google.maps.LatLngBounds();
    
    for (var key in dealerships) {
        
        // get current dealer info from array
        var data = dealerships[key];
        
        // info window content
        var sContent =
      	'<h4>' + data[2] + '</h4>' +
      	'<p>' + data[3] + '</p>';
        
        // add info window to map
        infoWindow = new google.maps.InfoWindow({ content: sContent });
        
        
        // add Marker to map
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng (data[0], data[1]),
          map: map,
          title: data[2],
          info: sContent
        });
        
                
        google.maps.event.addListener( marker, 'click', function() {
           infoWindow.setContent( this.info );
           infoWindow.open( map, this );
        
        });
          
        
        // expand map bounds to include all markers
        bounds.extend(new google.maps.LatLng (data[0], data[1]));

    } 
  
    map.initialZoom = true;
    map.fitBounds(bounds);
    
    }

google.maps.event.addDomListener(window, 'load', initialize);

 
</script>
   </body>
</html>
