<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package autocan-partners
 */
 
$auth = new Abcrc_Auth();

if (isset($_GET['login'])){
  $login_ok = $auth->login($_POST['Password']);
}

$logedin = $auth->is_authenticated();

if (isset($_GET['logout'])){
	$login_ok = $auth->logout();
}
 
//unset($_SESSION['entry_id']);

get_header(); ?>

<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
      <div class="container">
        <div class="panel">
        
        <?php if($logedin): ?>
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="row">
              <div class="col-xs-12">
                
                <h3><?php the_title(); ?></h3>
            
                <?php the_content(); ?>
                
                <?php if(isset($_SESSION['dealer_url'])):?>
                  <a href="<?=  $_SESSION['dealer_url'] ?>">View our page</a>
                <?php endif;?>
                
              </div>
            </div>
            
          <?php endwhile; endif; ?>
        <?php endif;?>
        
        </div>
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->

 
<?php get_footer(); ?>

