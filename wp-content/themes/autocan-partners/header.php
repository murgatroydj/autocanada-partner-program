<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package autocan-partners
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'autocan-partners' ); ?></a>
	<header id="masthead" class="site-header" role="banner">	
		<div class="site-branding container">
			<img src="<?= bloginfo('template_directory');?>/assets/img/DSS-AuotPartner-logo.jpg" height="70" style="height: 70px; margin: 10px 0px; width: auto" >
		</div><!-- .site-branding -->
	</header>
