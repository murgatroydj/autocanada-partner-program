<?php

/****************************************
Backend Functions
*****************************************/

/**
 * Register Widget Areas
 */
function om_widgets_init() {
  // Main Sidebar
  register_sidebar( array(
    'name'          => 'Sidebar Widget Area',
    'id'            => 'sidebar-widget-area',
    'description'   => 'Sidebar Widget Area',
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ) );
    
  // Footer
  register_sidebar( array(
    'name'          => 'Footer Widget Area',
    'id'            => 'footer-widget-area',
    'description'   => 'Footer Widget Area',
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ) );
  
   // Contact Page
  register_sidebar( array(
    'name'          => 'Contact Page Sidebar',
    'id'            => 'contact-widget-area',
    'description'   => 'Contact Widget Area',
    'before_widget' => '<div class="widget %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ) );

}


/**
 * Don't Update Theme
 * @since 1.0.0
 *
 * If there is a theme in the repo with the same name,
 * this prevents WP from prompting an update.
 *
 * @author Mark Jaquith
 * @link http://markjaquith.wordpress.com/2009/12/14/excluding-your-plugin-or-theme-from-update-checks/
 *
 * @param array $r, request arguments
 * @param string $url, request url
 * @return array request arguments
 */
function om_dont_update_theme( $r, $url ) {
  if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
    return $r; // Not a theme update request. Bail immediately.
  $themes = unserialize( $r['body']['themes'] );
  unset( $themes[ get_option( 'template' ) ] );
  unset( $themes[ get_option( 'stylesheet' ) ] );
  $r['body']['themes'] = serialize( $themes );
  return $r;
}




/**
 * Change Admin Menu Order
 */
function om_custom_menu_order( $menu_ord ) {
  if ( !$menu_ord ) return true;
  return array(
    // 'index.php', // Dashboard
    // 'separator1', // First separator
    // 'edit.php?post_type=page', // Pages
    // 'edit.php', // Posts
    // 'upload.php', // Media
    // 'gf_edit_forms', // Gravity Forms
    // 'genesis', // Genesis
    // 'edit-comments.php', // Comments
    // 'separator2', // Second separator
    // 'themes.php', // Appearance
    // 'plugins.php', // Plugins
    // 'users.php', // Users
    // 'tools.php', // Tools
    // 'options-general.php', // Settings
    // 'separator-last', // Last separator
  );
}

/**
 * Hide Admin Areas that are not used
 */
function om_remove_menu_pages() {
  // remove_menu_page('link-manager.php');
}

/**
 * Disable Emojicons
 */

function flush_emojicons() {
    // Remove from comment feed and RSS
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // Remove from Emails
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

    // Remove from the head, i.e. wp_head()
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

    // Remove from Print-related CSS
    remove_action( 'wp_print_styles', 'print_emoji_styles' );

    // Remove from Admin area
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
}
add_action( 'init', 'flush_emojicons' );

/**
 * Remove default link for images
 */
function om_imagelink_setup() {
  $image_set = get_option( 'image_default_link_type' );
  if ($image_set !== 'none') {
    update_option('image_default_link_type', 'none');
  }
}

/**
 * Show Kitchen Sink in WYSIWYG Editor

function om_unhide_kitchensink( $args ) {
  $args['wordpress_adv_hidden'] = false;
  return $args;
}

 */
/**
 * Display featured image thumbnail in post admin columns
 */
// Setup Admin Thumbnail Size
if( function_exists( 'add_theme_support' ) ) {
  add_image_size( 'admin-thumb', 48, 48, true );
}

// Thumbnails to Admin Post View
add_filter( 'manage_posts_columns', 'posts_columns', 5 );
add_action( 'manage_posts_custom_column', 'posts_custom_columns', 5, 2 );



set_post_thumbnail_size( 50, 50 );


function posts_columns($defaults){
  $defaults['post_thumb'] = __( 'Thumbnail' );
  return $defaults;
}

function posts_custom_columns($column_name, $id){
  if( $column_name === 'post_thumb' ) {
    the_post_thumbnail( 'admin-thumb' );
  }
}


/**
 * Use the first attached image as featured image
 */
/*function om_set_attachment_as_featured_image() {
  global $post;

  if( ! has_post_thumbnail( $post->ID ) ) {
    $attached_image = get_children( array(
      'post_parent' => $post->ID,
      'post_status' => 'inherit',
      'post_type' => 'attachment',
      'post_mime_type' => 'image',
      'numberposts' => 1
    ) );

    if( $attached_image ) {
      // If it has an image, set it as the featured image
      foreach( $attached_image as $attachment ) {
        set_post_thumbnail( $post->ID, $attachment->ID );
      }
    }
  }
}
add_action( 'the_post', 'om_set_attachment_as_featured_image' );
add_action( 'save_post', 'om_set_attachment_as_featured_image' );
add_action( 'draft_to_publish', 'om_set_attachment_as_featured_image' );
add_action( 'new_to_publish', 'om_set_attachment_as_featured_image' );
add_action( 'pending_to_publish', 'om_set_attachment_as_featured_image' );
add_action( 'future_to_publish', 'om_set_attachment_as_featured_image' );*/


/**
 * Change login screen logo
 */
function om_custom_login_logo() {
  echo '<style type="text/css">
    .login h1 a {
      width: 173px;
      background-image: url(' . get_bloginfo('template_directory') . '/assets/images/logo--admin.png) !important;
      background-image: none, url(' . get_bloginfo('template_directory') . '/assets/img/adminlogo.png) !important;
      -webkit-background-size: contain;
      background-size: contain;
    }
    </style>';
}
function om_custom_login_url() {
  return( 'http://jamesmurgatroyd.com/' );
}
function om_custom_login_title() {
  return( 'From your friends at James Murgatroyd Communications' );
}
add_action( 'login_head', 'om_custom_login_logo' );
add_filter( 'login_headerurl', 'om_custom_login_url' );
add_filter( 'login_headertitle', 'om_custom_login_title' );




/****************************************
Frontend
*****************************************/

/**
 * Enqueue scripts
 */
function om_scripts() {
  // CSS first
  wp_enqueue_style( 'theme_style', get_template_directory_uri().'/assets/css/styles.min.css', null, '1.0', 'all' );
  
  // JavaScript
  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
  if ( !is_admin() ) {
  wp_deregister_script('jquery'); // Remove WordPress core's jQuery
  wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js', false, null, false);
  // add_filter('script_loader_src', 'theme_jquery_local_fallback', 10, 2);
  //wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr.min.js', false, false );
    
    wp_enqueue_script( 'masonry', "http://cdnjs.cloudflare.com/ajax/libs/masonry/3.1.5/masonry.pkgd.min.js", array('jquery'), NULL, true );
    wp_enqueue_script( 'customplugins', get_template_directory_uri() . '/assets/js/plugins.min.js', array('jquery'), NULL, true );
    wp_enqueue_script( 'customscripts', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery'), NULL, true );
  }
}


/**
 * Improved caption shortcode output
 *
 * @param  string $output
 * @param  array  $attr
 * @param  string $content
 * @return string $output
 */
function om_caption_construct( $output, $attr, $content ) {
  if( is_feed() ) {
    return $output;
  }

  $defaults = array(
    'id' => '',
    'align' => 'alignnone',
    'width' => '',
    'caption' => ''
  );
  $attr = shortcode_atts( $defaults, $attr );

  if( 1 > $attr['width'] || empty( $attr['caption'] ) ) {
    return $content;
  }

  if( $attr['id'] ) {
    $id = esc_attr( $attr['id'] );
  }

  $content = str_replace( '<img', '<img itemprop="contentURL"', $content );

  $output = '<figure class="wp-caption ' . esc_attr( $attr['align'] ) . '" id="' . $id . '" aria-describedby="figcaption-' . $id . '"  itemscope itemtype="http://schema.org/ImageObject">';
  $output .= do_shortcode( $content );
  $output .= '<figcaption class="wp-caption-text" id="figcaption-' . $id . '" itemprop="desciption"><p>' . $attr['caption'] . '</p></figcaption>';
  $output .= '</figure>';

  return $output;
}


/**
 * If there's a Featured Image on a post, get it
 *
 * @return featured image url
 */
function om_get_featured_image( $size = 'large' ) {
  if ( has_post_thumbnail() ) {
    $img = wp_get_attachment_image_src( get_post_thumbnail_id(), $size );

    return $img[0];
  }
}


/**
 * Get the posterframe image from a video url.
 *
 * 1. YouTube thumbnail size [default, 0, 1, 2, 3]
 * 2. Vimeo thumbnail size [thumbnail_small, thumbnail_medium, thumbnail_large]
 *
 * @param video url
 * @return posterframe image url
 */
function om_get_video_poster_frame( $url ) {
  $image_url = parse_url( $url );

  // YouTube video
  if( $image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com' ) {

    $array = explode( '&', $image_url['query'] );
    return 'http://img.youtube.com/vi/' . substr( $array[0], 2 ) . '/0.jpg';

  // YouTube shorturl
  } else if( $image_url['host'] == 'www.youtu.be' || $image_url['host'] == 'youtu.be' ) {

    $array = explode('/', $image_url['path']);
    return 'http://img.youtube.com/vi/' . $array[1] . '/0.jpg';

  // Vimeo video
  } else if( $image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com' ) {

    $hash = unserialize( file_get_contents( 'http://vimeo.com/api/v2/video/' . substr( $image_url['path'], 1 ) . '.php' ) );
    return $hash[0]['thumbnail_small'];

  }
}


/**
 * Check if current page has children
 */
function has_children() {
  global $post;

  $children = get_pages( array( 'child_of' => $post->ID ) );
  if( $children ) {
    return true;
  } else {
    return false;
  }
}


/**
 * Check if the current page has children or a parent page
 *
 * @param current page ID
 * @return boolean
 */
function is_tree( $pid ) {
  // load details about this page
  global $post;

  if( is_page() && ( $post->post_parent || has_children() ) ) {
    // we're at the page or at a sub page
    return true;

  } else {

    // we're elsewhere
    return false;
  }
}


/**
 * Get the children or sibling pages
 */
function get_siblings() {
  global $post;

  add_filter('wp_list_pages', create_function('$t', 'return str_replace("<a ", "<a class=\"subnav-link\" ", $t);'));

  if( $post->post_parent ) {

    // has a parent page
    $siblings = wp_list_pages( 'title_li=&child_of=' . $post->post_parent . '&echo=0' );

  } else {

    // is a parent page
    $siblings = wp_list_pages( 'title_li=&child_of=' . $post->ID . '&echo=0' );
  }

  if( $siblings ) {

    echo $siblings;

  }
}


/**
 * Replace default excerpt ellipsis
 */
function om_replace_ellipsis( $text ) {
  $output = str_replace( '[&hellip;]', '&hellip;', $text );
  return $output;
}


/**
 * Get an array of meta info of a media attachment.
 *
 * @param integer
 * @return array
 */
 function om_get_attachment_meta( $attachment_id ) {
   $attachment = get_post( $attachment_id );
   $thumbnail = wp_get_attachment_image_src( $attachment->ID, 'thumbnail' );
   $medium = wp_get_attachment_image_src( $attachment->ID, 'medium' );

   return array(
    'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
    'caption' => $attachment->post_excerpt,
    'description' => $attachment->post_content,
    'src' => $attachment->guid,
    'title' => $attachment->post_title,
    'thumbnail' => $thumbnail[0],
    'medium' => $medium[0],
  );
 }


/**
 * Add a class to `previous_posts_link` and `next_posts_link` output.
 */
function om_posts_link_attributes( $output ) {
  $attributes = 'class=""';

  return str_replace( '<a href=', '<a ' . $attributes . ' href=', $output );
}


/**
 * Remove Query Strings From Static Resources
 */
function om_remove_script_version( $src ){
  $parts = explode( '?', $src );
  return $parts[0];
}


/**
 * Remove Read More Jump
 */
function om_remove_more_jump_link( $link ) {
  $offset = strpos( $link, '#more-' );
  if ($offset) {
    $end = strpos( $link, '"',$offset );
  }
  if ($end) {
    $link = substr_replace( $link, '', $offset, $end-$offset );
  }
  return $link;
}


