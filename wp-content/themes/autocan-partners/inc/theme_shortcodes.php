<?php
// columned shortcode

function clean_content($html){
	
	//$pattern = "/<p[^>]*><\\/p[^>]*>/"; 
	$pattern = "#<p>(\s|&nbsp;|</?\s?br\s?/?>)*</?p>#"; // use this pattern to remove any empty tag

	return preg_replace($pattern, '', $html); 
}

function shortcode_button($atts, $content = null){
  extract(shortcode_atts(array( 'text' => 'Read More', 'url'=> 'http://', 'color' => 'blue'), $atts));
  
  $new_button = '<a href="'.$url.'" class="btn btn-push btn-'. $color.'" >' . $text . '</a>';

	return $new_button;
  
}

add_shortcode('button', 'shortcode_button');

?>