<?php 
  
//store session for entry_id;
function update_form_meta( $field_id, $meta_value){
  if(isset($_SESSION["entry_id"])){
    $frm_entry_meta = new FrmEntryMeta();
    $frm_entry_meta->update_entry_meta($_SESSION["entry_id"], $field_id, $meta_key = null,$meta_value);
  } else {
   echo "Could not save data";
  }
}

//Get Featured Image URL 
function wp_get_thumbnail_url($id){
	if(has_post_thumbnail($id)){
		$imgArray = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'single-post-thumbnail' );
		$imgURL = $imgArray[0];
		return $imgURL;
	}else{
		return false;	
	}
}

// Email Function
require_once(get_template_directory() . '/inc/class.phpmailer.php');

function sendEmail($p_subject, $p_msg, $p_email, $p_cc = '',  $p_attachment =''){
	
	
	$email  = "<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><link href='http://partnerprogram.staging.wpengine.com/_email/email.css' rel='stylesheet'></head>
<body style=''><table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#eeeeee'><tr><td align='center'><table width='585' border='0' cellspacing='0' cellpadding='0'>
<tr><td class='header' height='107' align='right' valign='bottom' colspan='2' style='padding-top:20px;padding-bottom:0;padding-right:0;padding-left:0;background-color:#8dc63f;height:107px;'><img src='http://partnerprogram.staging.wpengine.com/_email/email-header.jpg' width='585' height='107' style='border-style:none;' /></td></tr>
<tr><td class='wrapper' bgcolor='#FFFFFF' colspan='2' valign='top' style='background-color:#FFF;'><table width='545' border='0' align='center' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' class='content' style='border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#c1d72e;'>
<tr><td align='left' valign='top' class='mainbar' style='padding-top:20px;padding-bottom:20px;padding-right:15px;padding-left:15px;'>     
   
{$p_msg}

</td></tr></table></td></tr><tr><td width='187' height='92' align='left' bgcolor='#FFFFFF'><p style='font-size:10px;color:#004812 !important;padding-left:20px;'> </td>
<td width='330' align='right' height='92' bgcolor='#FFFFFF'></td>
</tr></table></td></tr></table></body></html>";
	
	$result = false;
	
	$mail = new phpmailer();
	
	$mail->IsSMTP(); // use this for local testing
	
	$mail->Host     = 'mail.shaw.ca'; // use this for local testing
  
  $mail->SMTPAuth   = true;                  // enable SMTP authentication
  $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
  //$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
  //$mail->Username   = "yourname@yourdomain"; // SMTP account username
  //$mail->Password   = "yourpassword";       
	
	//$mail->IsMail(true); 
	
	//$mail->IsSendMail(true);
	$mail->IsHTML(true);
	
	$mail->From     = "james@jamesmurgatroyd.com";
	$mail->FromName = "James Murgatroyd";
	
	$mail->AddAddress($p_email);
	
	if($p_cc){
		$mail->AddCC($p_cc);
	}
	
	$mail->AddBCC("murgatroydj@shaw.ca");
	//$mail->AddBCC("monitor@can1.ca");
	$mail->Subject  =  $p_subject;
	
	if(isset($p_attachment)){
		$mail->AddAttachment ($p_attachment);
	}
	
	$mail->Body     = $email;
	
	if(!$mail->Send()) {
    $result = false;
    //$_SESSION['mail_error'] = $mail->ErrorInfo;
  } else {
    $result = true;
  }
  
  return $result;
  
}


function create_offer_certificate( $p_user_email, $p_dealer_id ){
  
  $dealer = null;
  $dealer_url = "";
  $dealer= get_post( $p_dealer_id);
  $logo =  wp_get_thumbnail_url( $p_dealer_id );
  $dealer_name = $dealer->post_title;
  $dealer_email =   get_field( 'email_address', $p_dealer_id);
  $dealer_contact = get_field( 'contact_name', $p_dealer_id); 
  $dealer_address = get_field( 'address', $p_dealer_id);
  $dealer_city = get_field( 'city', $p_dealer_id);
  $dealer_province = get_field( 'province', $p_dealer_id);
  $dealer_postal_code = get_field( 'postal_code', $p_dealer_id);
  $dealer_url = get_field( 'url', $p_dealer_id);
  $dealer_phone = get_field( 'phone', $p_dealer_id);
	
	$ty_content =  get_post( '216');
	$ty_subject = $ty_content->post_title;
	$ty_message = $ty_content->post_content;
	
	$result = false;
	
	$upload_dir = wp_upload_dir(); 
	
	require_once( get_template_directory() . '/inc/fpdf17/fpdf.php');
	require_once( get_template_directory() . '/inc/fpdf17/fpdi.php');
	
	$pdf = new FPDI('l', 'pt', 'letter');
	
	// add a page 
	$pdf->AddPage(); 
	$pdf->SetMargins(10, 10, 10);
	$pdf->SetAutoPageBreak(false);
	// set the sourcefile 
	$pdf->setSourceFile( get_template_directory() . '/assets/pdf/DSS_PARTNERPROGRAM.pdf'); 
	// import page 1 
	$tplIdx = $pdf->importPage(1); 
	// use the imported page and place it at point 10,10 with a width of 100 mm 
	$pdf->useTemplate($tplIdx, 0, 0, 0); 
	 
  // Add Dealer Logo to PDF
  $pdf->SetXY(530, 80); // 108
  //Image(file, x,  y, w, h, type, link)
  $pdf->Image($logo, $pdf->GetX(), $pdf->GetY(), 200, '' , "", $dealer_url);
  
  $deal_address  = strtoupper($dealer_address . "\n" . $dealer_city . ", " . $dealer_province. "  " . $dealer_postal_code);
  if(!empty($dealer_phone)){$deal_address .= "\n" . $dealer_phone;}
  if(!empty($dealer_url)){$deal_address .= "\n" . $dealer_url;}
  
  
  // Add Dealer Address Block
  $pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(0,0,0);
	$pdf->SetXY(530, 170); // 108
	//$pdf->SetFillColor(147,149,152);
	//MultiCell( w,  h,txt , border , align , fill)
	$pdf->MultiCell(200,14, $deal_address, 0,'L'); 
  
	// create cells of text over image
	
	$name = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 95, true) . " " . FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 104, true);
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	//$pdf->SetFillColor(237,28,36);
	$pdf->SetXY(145, 302); // 108 
	//Cell(  w, h, txt, border, ln, align, fill, link)
	$pdf->Cell(240,36, strtoupper($name) ,0,1,'L');
	
	$phone = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 105, true);
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	$pdf->SetXY(145, 332); // 108 
	$pdf->Cell(240,36, strtoupper($phone) ,0,1,'L'); 
	
	$email = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 11, true);
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	$pdf->SetXY(145, 354); // 108 
	$pdf->Cell(240,36,strtoupper($email) ,0,1,'L'); 
	
	$address  = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 114, true);
  $address .= "\n";
  if(!empty(FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 115, true))){
    $address .= FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 115, true);
    $address .= "\n";
  }
  $address .= FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 116, true);
  $address .= " ";
  $address .= FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 117, true);
  $address .= " ";
  $address .= FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 118, true);
  
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	$pdf->SetXY(145, 390);
	//MultiCell( w,  h,txt , border , align , fill)
	$pdf->MultiCell(240,11, ($address) ,0,'L', false); 
		
	
	$parter = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 103, true);
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	$pdf->SetXY(540, 302);
	$pdf->Cell(240,36, strtoupper($parter) ,0,1,'L');
	
	$dealer = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 99, true);
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	$pdf->SetXY(540, 325.5);
	$pdf->Cell(240,36, strtoupper($dealer) ,0,1,'L');

  $brand = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 96, true);
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	$pdf->SetXY(540, 350);
	$pdf->Cell(240,36, strtoupper($brand) ,0,1,'L');
	
	$offer = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 101, true);
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	$pdf->SetXY(540, 375);
	$pdf->Cell(240,36, strtoupper($offer) ,0,1,'L');
	
	$date =  date("d.m.Y");  
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	$pdf->SetXY(540, 398);
	$pdf->Cell(240,36, strtoupper($date) ,0,1,'L');
	
	$date2 =  date("m/d/Y"); 
  $expire = date("d.m.Y",strtotime($date2 . "+7 days"));
	$pdf->SetFont('helvetica', '', 11);
	$pdf->SetTextColor(237,28,36);
	$pdf->SetXY(540, 421);
	$pdf->Cell(240,36, strtoupper($expire) ,0,1,'L');
  
  $file_name = 'Auto-Canada-Partner-Offer' .  time() . '.pdf';
	
	// save file to uploads dir
	$file = $upload_dir['basedir'] . '/offers/' . $file_name;
	$link =  $upload_dir['baseurl'] . '/offers/' . $file_name;
	update_form_meta( '120',  $link);
	
	$pdf->Output($file , 'F'); 
	$pdf->Close();
	
	$user_email= FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 11, true);

	
	// Test Override
	$dealer_email = 'murgatroydj@shaw.ca';
	//$p_user_email = 'murgatroydj@shaw.ca';
	
	sendEmail($ty_subject, $ty_message , $user_email, $dealer_email, $file);	
	
	flush();
	return $result;
}


function after_entry_created($entry_id, $form_id){
  
  if(!isset($_SESSION)) {
     session_start();
  }
   
  $_SESSION["entry_id"] =  $entry_id;
   
  if(isset($_SESSION["entry_id"]) && isset($_SESSION['partner_name'])){
    update_form_meta( '103', $_SESSION['partner_name']);
  }
  if(isset($_SESSION["entry_id"]) && isset($_SESSION['brand'])){
    update_form_meta( '96',  $_SESSION['brand']);
  } 
  if(isset($_SESSION["entry_id"]) && isset($_SESSION['brand_id'])){
    update_form_meta( '110', $_SESSION['brand_id']);
  }
  if(isset($_SESSION["entry_id"]) && isset($_SESSION['offer_name'])){
    update_form_meta( '101',   $_SESSION['offer_name']);  
  }
  if(isset($_SESSION["entry_id"]) && isset($_SESSION['dealer_id'])){
    update_dealer_form_meta( $_SESSION['dealer_id']);
  }
}


function update_dealer_form_meta($p_dealer_id){
  if($p_dealer_id){
  
  $sites = get_post($p_dealer_id);
     
  $name =$sites->post_title;
  $email =   get_field( 'email_address', $p_dealer_id );
  $url =   get_field( 'url', $p_dealer_id );
  $contact = get_field( 'contact_name', $p_dealer_id );
  
 
  update_form_meta( '100', $p_dealer_id );
  update_form_meta( '99', $name );
  update_form_meta( '111', $email );
  update_form_meta( '112', $url );
  update_form_meta( '113', $contact );  
  
  $_SESSION['dealer_url'] = $url;
}

}


add_action('frm_after_create_entry', 'after_entry_created', 30, 2);


function remove_page_excerpt_field() {
	remove_meta_box( 'postexcerpt' , 'page' , 'normal' ); 
}

add_action( 'admin_menu' , 'remove_page_excerpt_field' );

function remove_post_excerpt_field() {
	remove_meta_box( 'postexcerpt' , 'post' , 'normal' ); 
}
add_action( 'admin_menu' , 'remove_post_excerpt_field' );

function remove_cpt_excerpt_field() {
	remove_meta_box('postexcerpt' , 'assessment', 'normal' );
}
add_action( 'admin_menu', 'remove_cpt_excerpt_field' );

function om_shorten_string($p_string = "", $p_limit = 10){
  $result = implode(' ', array_slice(explode(' ', $p_string), 0, $p_limit)) . "..."; 
  $result = strip_tags($result);
  return $result;
}

 
if(isset($_GET['dev'])){
	add_action( 'wp_footer', 'wp_footer_example' );
	function wp_footer_example() {
	    $stat = sprintf( '%d queries in %.3f seconds, using %.2fMB memory',
	        get_num_queries(),
	        timer_stop( 0, 3 ),
	        memory_get_peak_usage() / 1024 / 1024
	    );
	    if( current_user_can( 'manage_options' ) ) {
	        echo '<span class="stats" style="display: block;position: fixed;bottom: 0;left: 0;background: #fff;color: #f00;padding: 10px;">' . $stat . '</span>';
	    }
	}
}
 
?>