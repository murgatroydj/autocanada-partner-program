<?php

if ( ! function_exists( 'jmc_v3_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function jmc_v3_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on autocan-partners, use a find and replace
	 * to change 'autocan-partners' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'autocan-partners', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

  
	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	
	
	/****************************************
  Frontend
  *****************************************/
 
  // Add custom excerpt field to Pages
  add_post_type_support( 'page', 'excerpt' );

  add_theme_support( 'post-thumbnails' );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'jmc_v3_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );


  /****************************************
  Backend
  *****************************************/

  // Clean up the head
  remove_action( 'wp_head', 'rsd_link' );
  remove_action( 'wp_head', 'wlwmanifest_link' );
  remove_action( 'wp_head', 'wp_generator' );
  remove_action( 'wp_head', 'wp_shortlink_wp_head' );

  // Register menus
  register_nav_menus( array(
    'top-menu' => 'Top Menu',
    'main-menu' => 'Main Menu',
    'footer-menu' => 'Footer Menu'
  ) );

  // Register Widget Areas
  add_action( 'widgets_init', 'om_widgets_init' );

  // Execute shortcodes in widgets
  // add_filter('widget_text', 'do_shortcode');

  
  // Add Editor Style
  //add_editor_style( get_template_directory_uri() . '/assets/css/editor-style.css' );
  
  /* Add body class to Visual Editor to match class used live
    function mytheme_mce_settings( $initArray ){
     $initArray['body_class'] = 'article mod-content';
     return $initArray;
    }
    add_filter( 'tiny_mce_before_init', 'mytheme_mce_settings' );
    
    add_filter( 'tiny_mce_before_init', 'custom_mce_before_init' );
*/
   add_filter( 'http_request_args', 'om_dont_update_theme', 5, 2 );

  // Prevent File Modifications
 // define ( 'DISALLOW_FILE_EDIT', true );

  // Set Content Width
  if ( ! isset( $content_width ) ) $content_width = 900;

  

   // Change Admin Menu Order
  add_filter( 'custom_menu_order', 'om_custom_menu_order' );
  add_filter( 'menu_order', 'om_custom_menu_order' );

  // Hide Admin Areas that are not used
  add_action( 'admin_menu', 'om_remove_menu_pages' );

  // Remove default link for images
  add_action( 'admin_init', 'om_imagelink_setup', 10 );

  // Show Kitchen Sink in WYSIWYG Editor
  //add_filter( 'tiny_mce_before_init', 'om_unhide_kitchensink' );

  // Don't compress JPGs
  add_filter( 'jpeg_quality', function($arg){ return 100; } );
  add_filter( 'wp_editor_set_quality', function($arg) { return 100; } );


  

  // Remove width attribute from `figure` elements
  add_filter( 'img_caption_shortcode', 'om_caption_construct', 10, 3 );


  // Enqueue scripts
  add_action( 'wp_enqueue_scripts', 'om_scripts' );

  // Add posts links attributes
  add_filter( 'next_post_link', 'om_posts_link_attributes' );
  add_filter( 'previous_post_link', 'om_posts_link_attributes' );


  // Replace default excerpt ellipsis
  // add_filter( 'excerpt_more', '__return_false' );
  add_filter( 'the_excerpt', 'om_replace_ellipsis' );
  add_filter( 'get_the_excerpt', 'om_replace_ellipsis' );


  // Remove Query Strings From Static Resources
  add_filter( 'script_loader_src', 'om_remove_script_version', 15, 1 );
  add_filter( 'style_loader_src', 'om_remove_script_version', 15, 1 );

  // Remove Read More Jump
  add_filter( 'the_content_more_link', 'om_remove_more_jump_link' );


} // autocan-partnerssetup

endif; 

add_action( 'after_setup_theme', 'jmc_v3_setup' );