<?php
  
  // hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'om_register_taxonomies', 0 );

// create two taxonomies, genres and writers for the post type "book"
function om_register_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Brand', 'taxonomy general name' ),
		'singular_name'     => _x( 'Brand', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Brands' ),
		'all_items'         => __( 'All Brands' ),
		'parent_item'       => __( 'Parent Brand' ),
		'parent_item_colon' => __( 'Parent Brand:' ),
		'edit_item'         => __( 'Edit Brand' ),
		'update_item'       => __( 'Update Brand' ),
		'add_new_item'      => __( 'Add New Brand' ),
		'new_item_name'     => __( 'New Brand Name' ),
		'menu_name'         => __( 'Brand' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'case_study_cats' ),
	);

	register_taxonomy( 'brand_cats', array( 'dealers' ), $args );

}



 
/**
 * Display a custom taxonomy dropdown in admin
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'dealers'; // change to your post type
	$taxonomy  = 'brand_cats'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'dealers'; // change to your post type
	$taxonomy  = 'brand_cats'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}

?>