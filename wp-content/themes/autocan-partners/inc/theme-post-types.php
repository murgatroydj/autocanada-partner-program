<?php

/****************************************
Custom Post types
*****************************************/

/**
 * Hero Images post type
 */
function Dealer_cpt() {

	$labels = array(
		'name'                => 'Dealers',
		'singular_name'       => 'Dealer',
		'menu_name'           => 'Dealers',
		'parent_item_colon'   => 'Dealer:',
		'all_items'           => 'All Dealers',
		'view_item'           => 'View Dealer',
		'add_new_item'        => 'Add New Dealer',
		'add_new'             => 'Add New Dealer',
		'edit_item'           => 'Edit Dealer',
		'update_item'         => 'Update Dealer',
		'search_items'        => 'Search Dealers',
		'not_found'           => 'No Dealer found',
		'not_found_in_trash'  => 'Dealers not found in Trash',
	);
	$args = array(
		'label'               => 'Dealer',
		'description'         => 'Dealer posts',
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( '' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-testimonial',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'dealers', $args );

}

// Hook into the 'init' action
add_action( 'init', 'Dealer_cpt', 0 );


function offer_cpt() {

	$labels = array(
		'name'                => 'Offers',
		'singular_name'       => 'Offer',
		'menu_name'           => 'Offers',
		'parent_item_colon'   => 'Offer:',
		'all_items'           => 'All Offers',
		'view_item'           => 'View Offer',
		'add_new_item'        => 'Add New Offer',
		'add_new'             => 'Add New Offer',
		'edit_item'           => 'Edit Offer',
		'update_item'         => 'Update Offer',
		'search_items'        => 'Search Offers',
		'not_found'           => 'No Offer found',
		'not_found_in_trash'  => 'Offers not found in Trash',
	);
	$args = array(
		'label'               => 'Offer',
		'description'         => 'Offer posts',
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( '' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-testimonial',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( '0ffers', $args );

}

// Hook into the 'init' action
//add_action( 'init', 'offer_cpt',1 );

