<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package autocan-partners
 */

//session_start();
$step = 1;
$brand = '';
$entry_id =null;

// /print_r( $_SESSION);
//print_r( $_POST);

if(isset($_GET['brand']) && !empty($_GET['brand'])){
  $brand = $_GET['brand']; 
  $_SESSION['brand'] =  $_GET['brand'];  
}

if(isset($_SESSION['brand'])){
  $brand =$_SESSION['brand'];
}

if(isset($_GET['brandid'])){
  $_SESSION['brand_id'] = $_GET['brandid'];
}


if(isset($_GET['offer_name'])){
  $offer = $_GET['offer_name'];
  $_SESSION['offer_name'] = $_GET['offer_name'];   
}

if(isset($_GET['dealer_id'])){
  $_SESSION['dealer_id'] = $_GET['dealer_id'];
}


if(isset($_GET['step'])){
  $step = $_GET['step']; 
}

get_header(); ?>

<?php if($step == 1):?>

<section class="brand">
<div class="brand-wrap container">
<div class="panel">
   <div class="row">
      <div class="col-xs-12">
      
      <div class="jm-progress-wrap"> 
          <div class="jm-progress">
            
            <div class="circle active">
              <a href=""><span class="label">1</span></a>
            </div>
            
             <div class="title">
              CHOOSE YOUR BRAND
            </div>
              <div class="bar "></div>
          </div>
          <div class="jm-progress center">
            
            <div class="circle ">
              <a href="?step=1"><span class="label">2</span></a>
            </div>
            
             <div class="title">
             PICK A DEALER
            </div>
            
             <div class="bar done"></div>
            
          </div>
          <div class="jm-progress last">
            
            <div class="circle">
               <a href=""><span class="label">3</span></a>
            </div>
            
            <div class="title">
              CHOOSE YOUR BRAND
            </div>
              <div class="bar done"></div>
          </div>
        </div>
      
    </div>
    </div>
  <div class="row">
    <div class="col-md-12">
      <h1>1. Choose your brand</h1>
    </div>
  </div>
  <div class="row">
     <div class="col-md-12">
     <div id="grid" >
       <div id="posts">
        <?php 
        // no default values. using these as examples
        $taxonomies = array( 
            'brand_cats' 
        );
        
        $args = array(
            'orderby'           => 'name', 
            'order'             => 'ASC',
            'hide_empty'        => false
        ); 
        
        $terms = get_terms($taxonomies, $args);
        
        if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
            // echo '<ul class="brands">';
             
             foreach ( $terms as $term ) {
               $active ="";
               
               if ( isset($_SESSION['brand_id']) && $term->term_id == $_SESSION['brand_id'] ){ $active = 'active';}
                //echo   $id = $term->term_id;
                $getter =  "{$term->taxonomy}_{$term->term_id}";
                $image = get_field('brand_image',  $getter);
                $incentive_title = get_field('incentive_title', $getter);
               
                echo '<div class="post '.$term->name.' ' . $active .' dealer-link" ><a href="'. site_url().'/inside-page/?form=' . $entry_id . '&step=2&brand=' . $term->slug . '&brandid=' . $term->term_id . '&offer_name='.$incentive_title.'" ><div class="well"><div class="image"><img src="' . $image  . '"  /></div>';
             
                echo '<div class="foot"><h5>' . $incentive_title . '</h5></div></a>';
                // Start Deals
                
                //$dealer =  $term->term_id; 
                //$offers = get_posts(array(
    							//'post_type' => '0ffers',
    							//'meta_query' => array(
    							//	array(
    							//		'key' => 'Brand', // name of custom field
    								//	'value' => '"'. $dealer.'"', // matches exaclty "123", not just 123. This prevents a match for "1234"
    							//		'compare' => 'LIKE'
    						//		)
    						//	)
    						//));
    						//print_r($offers);
    						//if( $offers ){
  							 
  							  //foreach( $offers as $offer ){
    							  //print_r($offer);
    							 //echo '<h3>'.   get_the_title($offer->ID) . '</h3>';
    							
    							  //echo the_title( $offer->ID );
                   //$image = wp_get_thumbnail_url( $offer->ID); 
  							  //}
                //}
                // End Deals
                
                echo '</div> </div>';
             }
             
            // echo '</ul>';
         }
        
        ?>	
     </div>
     </div>
     </div>
    </div>
</div>
</div>
</section>
<?php endif;?>

<?php if($step == 2):?>
<section class="dealer">
<div class="dealer-wrap container">
 <div class="panel">
   
   <div class="row">
      <div class="col-xs-12">
      <div class="jm-progress-wrap"> 
          <div class="jm-progress">
            
            <div class="circle done">
              <a href="?step=1"><span class="label">1</span></a>
            </div>
            
             <div class="title">
              CHOOSE YOUR BRAND
            </div>
              <div class="bar "></div>
          </div>
          <div class="jm-progress center">
            
            <div class="circle active">
               <a href="?step=2"><span class="label">2</span></a>
            </div>
            
             <div class="title">
             PICK A DEALER
            </div>
            
             <div class="bar done"></div>
            
          </div>
          <div class="jm-progress last">
            
            <div class="circle">
               <a href=""><span class="label">3</span></a>
            </div>
            
            <div class="title">
              CHOOSE YOUR BRAND
            </div>
              <div class="bar done"></div>
          </div>
        </div>
    </div>
    </div>  
   <div class="row">
    <div class="col-md-12">
      <h1>2. Choose your Location</h1>
    </div>
  </div>
   
  <div class="row no-gutter">
      <div class="col-xs-12">
<!-- marker array -->
  <script>
   var dealerships = {
   
   <?php
    $type = 'dealers';
    
    $args=array(
      'post_type' => $type,
      'tax_query' => array(
    		array(
    			'taxonomy' => 'brand_cats',
    			'field'    => 'slug',
    			'terms'    => $brand,
    		),
      ),
    );
    
    $sites = null;
    $sites = new WP_Query($args);
          
      while( $sites->have_posts() ) : $sites->the_post();    
      
        $name = get_the_title();
        $address =  get_field('address');
        $city = get_field('city');
        $province =  get_field('province');
        $postal_code =  get_field('postal_code');
        $addy = $address . "<br />" . $city . " " . $province . " " .   $postal_code;
        $lat = trim( get_field('lat'));
        $long = trim(get_field('long'));
        
        echo "'$name':  [$lat, $long, '$name', '$addy'],"; 
         
      endwhile;  
    ?>
    }; 
  </script>	
     
    <div id="grid" >
       <div id="posts">
  
    <?php 
      
      while( $sites->have_posts() ) : $sites->the_post();    
        $active = '';
        if ( isset( $_SESSION['dealer_id']) && get_the_ID() == $_SESSION['dealer_id'] ){ $active = 'active';}
        $name = get_the_title();
        $name_url = urlencode ( $name  );
        $page = $_SERVER["PHP_SELF"];
        $id = get_the_ID();
        $email =  urlencode (trim(get_field('email_address')));
        $contact =  urlencode (trim(get_field('contact_name')));
        $logo =  wp_get_thumbnail_url( get_the_ID());
        $address =  get_field('address');
        $city = get_field('city');
        $province =  get_field('province');
        $postal_code =  get_field('postal_code');
        $URL = get_field('url');
        echo '<div class="post ' . $active . '">'."<a href='". site_url(). "/inside-page/$page?form={$entry_id}&step=3&dealer_id=$id' class='dealer-link'>" .'<div class="well">'; 
        echo "<div class='image'><img src='{$logo}' alt='{$name}'></div>";
        //echo "<h4> {$name} </h4>";
        echo "<div class='foot'><p>{$address}<br>{$city}, {$province}  {$postal_code}</p>";
        echo "<a href='{$URL}' target='_blank' class='btn btn-primary'>See Our Inventory</a>";
        echo "</div>";
        echo '</div></a></div>';
      
      endwhile;
    ?>
	  
	 </div>
 </div>
      </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
       <div id="map-canvas"></div>
    </div>
  </div>
 </div>
</div>
</section>			 
<?php endif;?>

<?php if($step == 3):?>



<section class="program">
<div class="program-wrap container">
  <div class="panel">
    <div class="row">
      <div class="col-xs-12">
      <div class="jm-progress-wrap"> 
          <div class="jm-progress">
            
            <div class="circle done">
             <a href="?step=1"><span class="label">1</span></a>
            </div>
            
             <div class="title">
              CHOOSE YOUR BRAND
            </div>
              <div class="bar "></div>
          </div>
          <div class="jm-progress center">
            
            <div class="circle done">
               <a href="?step=2"><span class="label">2</span></a>
            </div>
            
             <div class="title">
             PICK A DEALER
            </div>
            
             <div class="bar done"></div>
            
          </div>
          <div class="jm-progress last">
            
            <div class="circle active">
               <a href="?step=3"><span class="label">3</span></a>
            </div>
            
            <div class="title">
              CHOOSE YOUR BRAND
            </div>
              <div class="bar done"></div>
          </div>
        </div>
    </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
      <h1> 3. Review your choice: </h1>
      </div>
    </div>
  <div class="row">
    <div class="col-xs-12 col-md-6">
      
      <?php  
        
        //$brand_id = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 110, true); 
        $brand_id = $_SESSION['brand_id'];
        $args = array(
            'orderby'           => 'name', 
            'order'             => 'ASC',
            'hide_empty'        => false
        ); 
        
        $term =  get_term( $brand_id ,   'brand_cats');
        
        if ( ! empty( $term ) && ! is_wp_error( $term ) ){
          $active = ''; 
              
          $getter =  "{$term->taxonomy}_{$term->term_id}";
          $image = get_field('brand_image',  $getter);
          $incentive_title = get_field('incentive_title', $getter);
           
          echo '<div class="post '.$term->name.' ' . $active .'"><a href="'. site_url().'/inside-page/?form=' . $entry_id . '&step=1"><div class="well"><div class="image"><img src="' . $image  . '"  /></div>';
          echo '<div class="foot"><h5>' . $incentive_title . '</h5></div>';                
          echo '</div></a></div>';
        }
        
        ?>	
      
       </div>
  <div class="col-xs-12 col-md-6">
      
      <?php 
        
      //$dealer_id = FrmEntryMeta::get_entry_meta_by_field($_SESSION['entry_id'], 100, true); 
      $dealer_id = $_SESSION['dealer_id']; 
      
    $type = 'dealers';
    
    $args=array(
      'post_type' => $type,
      'tax_query' => array(
    		array(
    			'taxonomy' => 'brand_cats',
    			'field'    => 'slug',
    			'terms'    => $brand,
    		),
      ),
    );
    
    $sites = null;
    $sites = get_post( $dealer_id );
     
    $name = $sites->post_title;
    $page = $_SERVER["PHP_SELF"];
    $id = $dealer_id;
    $email =   get_field( 'email_address', $dealer_id);
    $url =   get_field( 'url', $dealer_id);
    $dealer_email = get_field( 'email_address', $dealer_id);
    $dealer_contact = get_field( 'contact_name', $dealer_id);

    
    
    echo '<div class="post">'."<a href='". site_url(). "/inside-page/$page?form=$entry_id&step=2' class='dealer-link'>" .'<div class="well">';
    $logo =  wp_get_thumbnail_url( $dealer_id );
    $address =  get_field( 'address', $dealer_id);
    $city = get_field('city', $dealer_id);
    $province =  get_field('province', $dealer_id);
    $postal_code =  get_field('postal_code', $dealer_id);
    echo "<div class='image' ><img src='{$logo}' alt='{$name}'></div>";
    //echo "<h4> {$name} </h4>";
    echo "<div class='foot'><p>{$address}<br>{$city}, {$province}  {$postal_code}</p>";
    echo "<a href='{$url}' target='_blank' class='btn btn-primary'>See Our Inventory</a>";
    echo "</div>";
    echo '</div></a></div>';
      
     
    ?>

      
  </div>
  </div>
    <div class="row">
      <div class="col-xs-12">
        <?php echo do_shortcode('[formidable id="4" title="1"]'); ?>
      </div>
    </div>
      </div>
     </div>

  </div>
</div>
</section>
<?php endif;?>	

<?php if($step == "complete"):?>

<?php

// output PDF and mail PDF
create_offer_certificate( $_SESSION["entry_id"] , $_SESSION['dealer_id'] );

?>
 
<script> 
  window.location.href = "<?php echo site_url(); ?>/index.php/thank-you/";
</script>

<?php endif;?>	
      </div>
		</main><!-- #main -->
	</div><!-- #primary -->
<hr>
 
<?php get_footer(); ?>
