// Load plugins
var gulp        = require('gulp');
var debug       = require('gulp-debug');
var del         = require('del');
var minifyCSS   = require('gulp-minify-css');
var size        = require('gulp-filesize');
var uncss       = require('gulp-uncss');
var filter      = require('gulp-filter');
var browserSync = require("browser-sync");
var reload      = browserSync.reload;
var argv        = require('yargs').argv;
var jpegoptim   = require('imagemin-jpegoptim');
var pngquant    = require('imagemin-pngquant');
var optipng     = require('imagemin-optipng');
var svgo        = require('imagemin-svgo');
var gm          = require('gulp-gm');
var bower       = require('gulp-bower');

var plugins = require('gulp-load-plugins')({ 
    camelize: true ,
     rename: {
      'gulp-ruby-sass': 'sass',
      'gulp-minify-css': 'minifyCSS',
      'gulp-filesize': 'size',
      'gulp-uncss': 'uncss'
    }
});

var config = {
    siteUrl: 'partners.autocan.dev',
    themePath: 'wp-content/themes/autocan-partners',
    assetPath: 'wp-content/themes/autocan-partners/assets',
    sassPath:  'wp-content/themes/autocan-partners/source/styles',
    bowerDir:  './wp-content/themes/autocan-partners/source/vendor'
}
 
var bsconfig = {
    // 1. inform browser-sync to watch compiled *.css files instead of *.scss sass files
    files: [ config.themePath + "/assets/css/*.css", "*.html", config.themePath + "/assets/css/*.js"],
    proxy:  config.siteUrl,
    logFileChanges: true,
    open: "local", //"local", "external", "ui", "ui-external" or "tunnel"
    notify: true,
    port: 3003 
}
    
gulp.task('browser-sync', function() {   
  browserSync(bsconfig);
});

gulp.task('bower', function() {
  return bower(config.bowerDir)
    .pipe(gulp.dest(config.bowerDir))
}); 


// Styles
gulp.task('styles', function () {
   return gulp.src(config.sassPath + '/styles.scss')
    
    .pipe(plugins.sass({
      loadPath: [
        config.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
        config.sassPath + '**/*.scss'
      ]
      
     }))
    .pipe(debug({title: 'unicorn:'}))
    .on('error', function (err) { console.log(err.message); })
    
    .pipe(gulp.dest(config.themePath + '/assets/css/'))
    .pipe(plugins.rename({ suffix: '.min' }))
  	.pipe(minifyCSS({keepBreaks:true}))
  	.pipe(gulp.dest(config.themePath +'/assets/css'))
    .pipe(plugins.notify({ message: 'Styles task complete' }))
});

gulp.task('editor_styles', function () {
   return gulp.src(config.sassPath + '/editor-style.scss')
    
    .pipe(plugins.sass({
       
      sourcemap: false, 
      sourcemapPath: '../scss',
      
      loadPath: [
        config.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
        config.sassPath + '**/*.scss'
      ]
      
     }))
    .on('error', function (err) { console.log(err.message); })
    .pipe(gulp.dest(config.themePath + '/assets/css/'))
    .pipe(plugins.rename({ suffix: '.min' }))
  	.pipe(minifyCSS({keepBreaks:true}))
  	.pipe(gulp.dest(config.themePath + '/assets/css'))
    .pipe(plugins.notify({ message: 'Editor Styles task complete' }))
});

gulp.task('modernizr', function() {
    return gulp.src(config.bowerDir + '/modernizr/modernizr.js')
        .pipe(require('gulp-modulizr')([
            'cssclasses',
            'svg',
            'url-data-uri'
        ]))
        .pipe(require('gulp-add-src')([
            config.bowerDir +  '/modernizr/feature-detects/url-data-uri.js'
        ]))
        .pipe(require('gulp-concat')('modernizr.js'))
        .pipe(gulp.dest(config.themePath + '/assets/js'))
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(plugins.uglify())
        .pipe(size())
        .pipe(gulp.dest(config.themePath + '/assets/js'))
        .pipe(plugins.notify({ message: 'Scripts task complete' }));
});

// Vendor Plugin Scripts
gulp.task('plugins', function() {
  return gulp.src([ 
    config.themePath + '/source/vendor/jquery/dist/jquery.js',
    config.themePath + '/source/vendor/bootstrap-sass-official/assets/javascripts/bootstrap.js',
    config.themePath + '/source/vendor/superfish/dist/js/hoverIntent.js',
    config.themePath + '/source/vendor/superfish/dist/js/superfish.min.js',
    config.themePath + '/source/vendor/fastclick/lib/fastclick.js',
    config.themePath + '/source/vendor/picturefill/dist/picturefill.js',
    config.themePath + '/source/vendor/imagesloaded/imagesloaded.pkgd.min.js',
    config.themePath + '/source/vendor/fitvids/jquery.fitvids.js',
    config.themePath + '/source/vendor/fancybox/source/jquery.fancybox.js',
    config.themePath + '/source/vendor/jquery.bxslider/jquery.bxslider.js',
    config.themePath + '/source/scripts/plugins.js',
  ])
	.pipe(plugins.concat('plugins.js'))
	.pipe(gulp.dest(config.themePath + '/assets/js/'))
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(plugins.uglify())

	.pipe(gulp.dest(config.themePath + '/assets/js'))
	.pipe(plugins.notify({ message: 'Scripts task complete' }));
	
	gulp.src([config.themePath + '/source/vendor/modernizr/modernizr.js'] )
  .pipe(gulp.dest(config.themePath + '/assets/js'));
});

// Site Scripts
gulp.task('scripts', function() {
  return gulp.src([config.themePath + '/source/scripts/*.js', '!' + config.themePath +'/source/scripts/plugins.js'])
	.pipe(plugins.jshint('.jshintrc'))
	.pipe(plugins.jshint.reporter('default'))
	.pipe(plugins.concat('main.js'))
	.pipe(gulp.dest(config.themePath + '/assets/js/'))
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(plugins.uglify())
	.pipe(gulp.dest(config.themePath + '/assets/js/'))
	.pipe(plugins.notify({ message: 'Scripts task complete' }));
});

gulp.task('images', function () {
    gulp.src(config.themePath + '/assets/img/**/*.{png,jpg,gif,svg}')
      .pipe(pngquant({ quality: '65-80', speed: 4 })())
      .pipe(optipng({ optimizationLevel: 4 })())
      //.pipe(jpegoptim({  progressive: true, max: 60})())
      .pipe(svgo()())
      .pipe(gulp.dest(config.themePath + '/assets/img/'))
      .pipe(plugins.notify({ message: 'Images task complete' }));
});


// Reload all Browsers
gulp.task('bs-reload', function () {
    browserSync.reload();
});


// Watch
gulp.task('watch', function () {
 
	// Watch .scss files
	gulp.watch(config.themePath + '/source/styles/**/*.scss', ['styles', 'editor_styles']);

	// Watch .js files
	gulp.watch(config.themePath + '/source/scripts/*.js', ['plugins', 'scripts', 'bs-reload']);

	// Watch image files
	gulp.watch(config.themePath + '/assets/images/*.png', ['images']);

  // php
  gulp.watch(config.themePath + '/**/**/*.php', ['bs-reload']);
  
  gulp.watch('*.html', ['bs-reload']);  
   

});

// Default task
gulp.task('default',  function() {
    gulp.start('styles','plugins', 'modernizr', 'scripts', 'images', 'watch', 'browser-sync');
});